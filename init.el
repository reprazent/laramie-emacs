;; Set some variables
(defvar laramie-root-dir
  (file-name-directory (or (buffer-file-name)
                           load-file-name))
  "Root directory of the laramie configuration (probably ~/.emacs.d/)")

(defvar laramie-core-file
  (expand-file-name "README.org" laramie-root-dir)
  "Core of the laramie configuration")

;; Initialize packages
(package-initialize)
(setq package-enable-at-startup nil)

;; Load org & org-babel
(require 'org)
(require 'ob-tangle)

;; Ignite org-babel magic
(org-babel-load-file laramie-core-file)
